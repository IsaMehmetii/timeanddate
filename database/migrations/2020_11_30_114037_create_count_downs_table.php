<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountDownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('count_downs', function (Blueprint $table) {
            $table->id();
            $table->string('design')->nullable();
            $table->string('title');
            $table->string('font_style')->nullable();
            $table->integer('day');
            $table->string('month');
            $table->integer('year');
            $table->integer('hour');
            $table->integer('minute');
            $table->integer('second');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countdowns');
    }
}
