<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimezonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timezones', function (Blueprint $table) {
            $table->id();
            $table->string('abbreviation')->nullable();
            $table->timestamp('datetime')->nullable();
            $table->string('minutes')->nullable();
            $table->integer('seconds')->nullable();
            $table->string('ampm')->nullable();
            $table->string('stringdate')->nullable();
            $table->string('timezone');
            $table->integer('unixtime')->nullable();
            $table->dateTime('utc_datetime', 0)->nullable();
            $table->string('utc_offset')->nullable();
            $table->smallInteger('week_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timezones');
    }
}
