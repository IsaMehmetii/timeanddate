<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('state_abbreviation')->nullable();
            $table->string('country_abbreviation')->nullable();
            $table->string('textual_id')->nullable();
            $table->unsignedBigInteger('timezone_id')->nullable();
            $table->unsignedBigInteger('continent_id')->nullable();
            $table->unsignedBigInteger('capital_id')->nullable();
            $table->string('name');
            $table->string('lat_long')->nullable();
            $table->string('elevation')->nullable();
            $table->string('currency')->nullable();
            $table->string('dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
