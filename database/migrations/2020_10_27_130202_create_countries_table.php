<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('abbreviation')->unique()->notNullable();
            $table->string('name');
            $table->string('textual_id')->nullable();
            $table->unsignedBigInteger('continent_id')->nullable();
            $table->string('long_name')->nullable();
            $table->string('capital_name')->nullable();
            $table->string('adm_capital')->nullable();
            $table->string('jud_capital')->nullable();
            $table->string('leg_capital')->nullable();
            $table->string('dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
