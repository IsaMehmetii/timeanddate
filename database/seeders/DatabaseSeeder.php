<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call('UsersTableSeeder');
         DB::table('users')->insert([
              'name' => Str::random(10),
             'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('password')
         ]);
    }
}
