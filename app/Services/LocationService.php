<?php 
namespace App\Services;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;

class LocationService
{	
	//Get country by textual_id
	public function getCountry($textual_id)
	{
		return Country::where('textual_id', $textual_id)->get();
	}
	//find country by abbreviation
	public function findCountry($abbr)
	{
		return Country::find($abbr);
	}
	
	//Get state by textual_id
	public function getState($textual_id)
	{
		return State::where('textual_id', $textual_id)->get();
	}
	//find state by abbreviation
	public function findState($abbr)
	{
		return State::find($abbr);
	}
	
	//Get city by textual_id
	public function getCity($textual_id)
	{
		return City::where('textual_id', $textual_id)->get();
	}
	
	//find city by id
	public function findCity($id)
	{
		return City::find($id);
	}


	//get All Countries
	public function getAllCountries()
	{
		$countries = Country::all();
	        $textual_id = array();
	        foreach ($countries as $country) {
	            array_push($textual_id,$country->textual_id);      
	        }
	        return response()->json($textual_id);
	}
	//get All States
	public function getAllStates()
	{
		 $states = State::all();
                $textual_id = array();
            foreach ($states as $state) {
                array_push($textual_id,$state->textual_id);      
            }
           return response()->json($textual_id);
	}
	//get All Cities
		public function getAllCities()
	{
		    $cities = City::all();
	        	$textual_id = array();
	        	foreach ($cities as $city) {
	        		array_push($textual_id,$city->textual_id);		
	        	}
				return response()->json($textual_id);
	}


}
