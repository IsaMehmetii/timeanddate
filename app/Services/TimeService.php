<?php 
namespace App\Services;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;

class TimeService
{

	public function realTime($timezone)
	{
		$dt = Carbon::now();
		$dt->timezone($timezone->abbreviation);
		$os = $dt->getOffset();
		$timezone->abbreviation = $dt->format('T');
		$timezone->datetime = $dt;
		$timezone->minutes = $dt->isoFormat('h:mm');
		$timezone->seconds = $dt->isoFormat('ss');
		$timezone->ampm = $dt->isoFormat('a');
		$timezone->stringdate = $dt->isoFormat('MMMM Do YYYY');
		$timezone->unixtime = $dt->getPreciseTimestamp(0);
		$dt->timezone('UTC');
		$timezone->utc_datetime = $dt;
		$timezone->utc_offset = $os >= 0?'+'.gmdate("G:i", $os):'-'.gmdate("G:i", -$os);
		$timezone->save();
    }
    public function storeTime($time, $timezone)
    {
    	$dt = Carbon::now();
		$dt->timezone($time);
		$os = $dt->getOffset();
		$timezone->abbreviation = $dt->format('T');
		$timezone->datetime = $dt;
		$timezone->minutes = $dt->isoFormat('h:mm');
		$timezone->seconds = $dt->isoFormat('ss');
		$timezone->ampm = $dt->isoFormat('a');
		$timezone->stringdate = $dt->isoFormat('MMMM Do YYYY');
		$timezone->timezone = $time;
		$timezone->unixtime = $dt->getPreciseTimestamp(0);
		$dt->timezone('UTC');
		$timezone->utc_datetime = $dt;
		$timezone->utc_offset = $os >= 0?'+'.gmdate("G:i", $os):'-'.gmdate("G:i", -$os);
    }

    public function getDuration($t1, $t2)
    {
    	return array(
            'startTime' => $t1->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
            'endTime' => $t2->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
            'years' => $t1->diffInYears($t2), 
            'months' => $t1->diffInMonths($t2),
            'weeks' => $t1->diffInWeeks($t2),
            'days' => $t1->diffInDays($t2),
            'hours' => $t1->diffInHours($t2),
            'minutes' => $t1->diffInMinutes($t2), 
            'seconds' => $t1->diffInSeconds($t2),
            'text' => $t1->diffForHumans($t2)
        );
    }
    public function parseStart($startDate)
    {
    	return  $startDate['year'].'-'.
	            $startDate['month'].'-'.
	            $startDate['day'].' '.
	            $startDate['hour'].':'.
	            $startDate['minute'].':'.
	            $startDate['second'];
    }
  
    //empty element
  function emptyElementExists($arr) {

    return array_search("", $arr) !== false;

  }

  function toTime($date, $dt){

  			if ($date['year'] == '') {
                $date['year'] = $dt->isoFormat('YYYY');
            }
            if ($date['month'] == '') {
                $date['month'] = $dt->isoFormat('MM');
            }
            if ($date['day'] == '') {
                $date['day'] = $dt->isoFormat('DD');
            }
              if ($date['hour'] == '') {
                $date['hour'] = $dt->isoFormat('H');
            }
              if ($date['minute'] == '') {
                $date['minute'] = $dt->isoFormat('m');
            }  
              if ($date['minute'] == '') {
                $date['minute'] = $dt->isoFormat('s');
            }
  }
 	 
}