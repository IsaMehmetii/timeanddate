<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCity extends Model {
	protected $fillable = [ 
		'user_id',
		'city_id',
	];
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	public function city()
	{
		return $this->belongsTo('App\Models\City');
	}
}