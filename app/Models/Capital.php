<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capital extends Model {
	protected $fillable = [ 
		'country_abbreviation',
		'state_abbreviation',
	];
	public function country()
	{
		return $this->belongsTo('App\Models\Country');
	}
	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}
	public function city()
	{
		return $this->hasMany('App\Models\City');
	}
}