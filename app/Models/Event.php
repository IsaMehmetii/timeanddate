<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
	protected $fillable = [ 
		'city_id',
		'user_id',
		'name',
		 'day', 
        'month',
        'year',
        'hour',
        'minute',
        'second',
         'hours',
        'minutes'
	];
	public function city()
	{
		return $this->belongsTo('App\Models\City');
	}
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
}