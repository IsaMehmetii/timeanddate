<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountDown extends Model {
		protected $fillable = [ 
		'city_id',
		'user_id',
		'design',
		'title',
		'font_style',
		'day',
		'month',
		'year',
		'hour',
		'minute',
		'second'
	];
	public function city()
	{
		return $this->belongsTo('App\Models\City');
	}
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	

}