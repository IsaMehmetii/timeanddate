<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model {

	 	protected $fillable = [
		'abbreviation',
		'client_ip',
		'datetime',
		'minutes',
		'seconds',
		'ampm',
		'stringdate',
		'timezone',
		'unixtime',
		'utc_datetime',
		'utc_offset',
		'week_number'
 	];

	 public function country()
    {
    	return $this->belongsToMany('App\Models\Country' , 'countries_timezones' , 'timezone_id' , 'country_abbreviation');
    }
      public function state()
    {
    	return $this->belongsToMany('App\Models\Country' , 'states_timezones' , 'timezone_id' , 'state_abbreviation');
    } 
      public function city()
    {
    	return $this->hasMany('App\Models\City');
    } 
}