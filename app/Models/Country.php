<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	protected $fillable = [ 
		'abbreviation',
		'continent_id',
		'name',
		'long_name',
		'textual_id',
		'capital_name',
		'dial_code'
	];
	public $keyType = 'string';
	public $incrementing = false;
	protected  $primaryKey = 'abbreviation';

	public function continent()
	{
		return $this->belongsTo('App\Models\Continent');
	}
	public function state()
	{
		return $this->hasMany('App\Models\State');
	}
	public function city()
	{
		return $this->hasMany('App\Models\City');
	}
	public function timezone()
   	{
   		return $this->belongsToMany('App\Models\timezone' , 'countries_timezones' , 'country_abbreviation' , 'timezone_id');
   	}
	public function capital()
	{
		return $this->hasOne('App\Models\Capital');
	}

}