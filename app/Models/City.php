<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {
		protected $fillable = [ 
		'state_code',
		'country_abbreviation',
		'textual_id',
		'continent_id',
		'capital_id',
		'name',
		'timezone_id',
		'lat_long',
		'elevation',
		'currency',
		'dial_codes'
	];
	public function country()
	{
		return $this->belongsTo('App\Models\Country');
	}
	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}
	public function timezone()
	{
		return $this->belongsTo('App\Models\Timezone');
	}
		public function continent()
	{
		return $this->belongsTo('App\Models\Continent');
	}
	public function capital()
	{
		return $this->belongsTo('App\Models\Capital');
	}
	public function userCity()
    {
        return $this->hasMany('App\Models\UserCity');
    }
}