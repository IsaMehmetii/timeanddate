<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model {
	protected $fillable = [
		'abbreviation',
		'country_abbreviation',
		'continent_id',
		'textual_id',
		'name',
		'type',
		'capital_name'
 	];
 	public $keyType = 'string';
	public $incrementing = false;
	protected  $primaryKey = 'abbreviation';
 	public function country()
	{
		return $this->belongsTo('App\Models\Country' , 'country_abbreviation');
	}
	public function city()
	{
		return $this->hasMany('App\Models\City');
	}

	public function timezone()
   	{
   		return $this->belongsToMany('App\Models\timezone' , 'states_timezones' , 'state_abbreviation' , 'timezone_id');
   	}	
	public function capital()
	{
		return $this->hasOne('App\Models\Capital');
	}
   	public function continent()
	{
		return $this->belongsTo('App\Models\Continent');
	}

}