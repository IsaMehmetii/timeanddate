<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Continents extends Model {
	
	protected $fillable = [ 
		'name',
		'short_name'
	];
	public function country()
	{
		return $this->hasMany('App\Models\Country');
	}
	public function state()
	{
		return $this->hasMany('App\Models\State');
	}
	public function city()
	{
		return $this->hasMany('App\Models\City');
	}
}