<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Continents;
use App\Models\City;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;

class ContinentController extends Controller
{   
    protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }

     public function cities($id)
     {  
       $country = Continents::find($id);
       $cities = $country->city;
       $result = array();
       foreach($cities as $city){
        array_push($result, $city->name);
       } 
       return response()->json($result);
     }
    
     public function file_get_content($url)
      {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        return $result ;
      } 

     public function pageTitle($result)
      {
        
        preg_match('/<title>(.*)<\/title>/', $result, $pageTitle);
        return strip_tags($pageTitle[1]);        
      } 

    public function pageCountry($result)
      { 

        preg_match('/<table class="table table--left table--inner-borders-rows">(.*)<\/table>/', $result, $pageCountry);
        dd($pageCountry);
        return strip_tags($pageCountry[1]);        
      }


     public function test($url)
     {
        $fullUrl = 'https://www.timeanddate.com/worldclock/'.$url;
        // dd($fullUrl);
        $result = $this->file_get_content($fullUrl);
        $pageTitle = $this->pageTitle($result);
        $pageCountry = $this->pageCountry($result);
        return response()->json($pageTitle);
     }
     
}

