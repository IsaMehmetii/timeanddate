<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use App\Models\City;
use App\Models\Timezone;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;


class DurationController extends Controller
{
protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }

     public function duration(Request $request)
    {      
         //validate request
        $this->validate($request, [
         'includeEndDate' => 'required|integer|min:0|max:1',
         'startDate.year' => 'required|integer|digits:4',
         'startDate.month' => 'required|date_format:m',
         'startDate.day' => 'required|integer|min:1|max:31',
         'endDate.year' => 'required|integer|digits:4',
         'endDate.month' => 'required|date_format:m',
         'endDate.day' => 'required|integer|min:1|max:31'
         
        ]);
        $endDate = $request->endDate;
        $startDate = $request->startDate;

        $t1 =Carbon::parse(
            $startDate['year'].'-'.
            $startDate['month'].'-'.
            $startDate['day']);
        $t1->addDays($request->includeEndDate);
        $t2 = Carbon::parse(
            $endDate['year'].'-'.
            $endDate['month'].'-'.
            $endDate['day']);
        
        $duration = $this->timeService->getDuration($t1, $t2);
        $includeEndDate = ['includeEndDate' => $request->includeEndDate];
        $duration = $duration + $includeEndDate;

            return response()->json($duration);
    }

        public function timeDuration(Request $request)
    {      
         //validate request
        $this->validate($request, [
         'startDate.year' => 'required|integer|digits:4',
         'startDate.month' => 'required|date_format:m',
         'startDate.day' => 'required|integer|min:1|max:31',
         'startDate.hour' => 'required|date_format:G',
         'startDate.minute' => 'required|numeric|min:0|max:59',
         'startDate.second' => 'required|numeric|min:0|max:59',
         'endDate.year' => 'required|integer|digits:4',
         'endDate.month' => 'required|date_format:m',
         'endDate.day' => 'required|integer|min:1|max:31',
         'endDate.hour' => 'required|date_format:G',
         'endDate.minute' => 'required|numeric|min:0|max:59',
         'endDate.second' => 'required|numeric|min:0|max:59',
         
        ]);
        $endDate = $request->endDate;
        $startDate = $request->startDate;

        $t1 =Carbon::parse(
            $startDate['year'].'-'.
            $startDate['month'].'-'.
            $startDate['day'].' '.
            $startDate['hour'].':'.
            $startDate['minute'].':'.
            $startDate['second']);
        $t2 = Carbon::parse(
            $endDate['year'].'-'.
            $endDate['month'].'-'.
            $endDate['day'].' '.
            $endDate['hour'].':'.
            $endDate['minute'].':'.
            $endDate['second']);
        
        $duration =  $this->timeService->getDuration($t1, $t2);

            return response()->json($duration);
    }

    public function timezoneDuration(Request $request){
          //validate request
        $this->validate($request, [
         'startCity_id' => 'required|exists:cities,id',            
         'endCity_id' => 'required|exists:cities,id',
         'startDate.year' => 'sometimes|integer|digits:4',
         'startDate.month' => 'sometimes|date_format:m',
         'startDate.day' => 'sometimes|integer|min:1|max:31',
         'startDate.hour' => 'sometimes|date_format:G',
         'startDate.minute' => 'sometimes|numeric|min:0|max:59',
         'startDate.second' => 'sometimes|numeric|min:0|max:59',
         'endDate.year' => 'sometimes|integer|digits:4',
         'endDate.month' => 'sometimes|date_format:m',
         'endDate.day' => 'sometimes|integer|min:1|max:31',
         'endDate.hour' => 'sometimes|date_format:G',
         'endDate.minute' => 'sometimes|numeric|min:0|max:59',
         'endDate.second' => 'sometimes|numeric|min:0|max:59',           
        ]);

        $startCity = City::find($request->startCity_id);
        $startTimezone= $startCity->timezone;
        $this->timeService->realTime($startTimezone);
        if (isset($request->startDate) && isset($request->endDate)) {

        $t1 = Carbon::parse($startTimezone->abbreviation)->startOfDay()
                      ->addDays($request->startDate['day'])
                      ->addMonths($request->startDate['month'])
                      ->addYears($request->startDate['year'])
                      ->addHours($request->startDate['hour']) 
                      ->addMinutes($request->startDate['minute']) 
                      ->addSeconds($request->startDate['second']); 

        $endCity = City::find($request->endCity_id);
        $endTimezone = $endCity->timezone;
        $this->timeService->realTime($endTimezone);
        $t2 = Carbon::parse($endTimezone->abbreviation)->startOfDay()
                      ->addDays($request->endDate['day'])
                      ->addMonths($request->endDate['month'])
                      ->addYears($request->endDate['year'])
                      ->addHours($request->endDate['hour']) 
                      ->addMinutes($request->endDate['minute']) 
                      ->addSeconds($request->endDate['second']); 

        $duration =  $this->timeService->getDuration($t1, $t2);
        return response()->json($duration);
        }else{
         return response()->json(['message'=> 'something went wrong']); 
        }


    }

     //calculate difference including or excluding
    public function workDaysDuration(Request $request)
    {
        //type - 0-include 1-exclude
        //arch - 1-weekends 2-allDays 3-custon
             //validate request
        $this->validate($request, [
         'includeEndDate' => 'required|integer|min:0|max:1',
         'startDate.year' => 'required|integer|digits:4',
         'startDate.month' => 'required|date_format:m',
         'startDate.day' => 'required|integer|min:1|max:31',
         'endDate.year' => 'required|integer|digits:4',
         'endDate.month' => 'required|date_format:m',
         'endDate.day' => 'required|integer|min:1|max:31'
         
        ]);
        $endDate = $request->endDate;
        $startDate = $request->startDate;

        $t1 =Carbon::parse(
            $startDate['year'].'-'.
            $startDate['month'].'-'.
            $startDate['day'])->subWeekday();
        $t1->addDays($request->includeEndDate);
        $t2 = Carbon::parse(
            $endDate['year'].'-'.
            $endDate['month'].'-'.
            $endDate['day'])->subWeekday();
         
        if ($request->type == 1) {
      
            if($request->ach == 1){
                $duration = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                }, $t2);
                $skipped = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isWeekend();
                }, $t2);
                $saturdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSaturday();
                }, $t2);
                $sundays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSunday();
                }, $t2);
                $duration = [
                    'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                    'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                    'includeEndDate' => $request->includeEndDate,
                    'calendar_days' => $t1->diffInDays($t2),
                    'duration' => $duration,
                    'skipped' => $skipped,
                    'saturdays' => $saturdays,
                    'sundays' => $sundays,
                    'type' => 'exclude',
                    'ach' => 'weekends'
                ];
            }else if ($request->ach == 2) {
                 $duration = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isSameDay($date);
                }, $t2);
                $sundays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSunday();
                }, $t2);
                 $mondays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isMonday();
                }, $t2);
                $tuesdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isTuesday();
                }, $t2);
                $wednesdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isWednesday();
                }, $t2);
                $thursdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isThursday();
                }, $t2);
                $fridays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isFriday();
                }, $t2);
                $saturdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSaturday();
                }, $t2);
                $duration = [
                    'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                    'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                    'includeEndDate' => $request->includeEndDate,
                    'calendar_days' => $t1->diffInDays($t2),
                    'duration' => $duration,
                    'skipped' => $t1->diffInDays($t2),
                    'sundays' => $sundays,
                    'mondays' => $mondays,
                    'tuesdays' => $tuesdays,
                    'wednesdays' => $wednesdays,
                    'thursdays' => $thursdays,
                    'fridays' => $fridays,
                    'saturdays' => $saturdays,
                    'type' => 'exclude',
                    'ach' => 'alldays'
                ];
            }else if ($request->ach == 3) {
                $custom = $request->custom;
                $skipped = 0;
                $duration = [
                    'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                    'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                    'includeEndDate' => $request->includeEndDate,
                    'calendar_days' => $t1->diffInDays($t2),
                    'type' => 'exclude',
                    'ach' => 'custom'
                ];
                if ($custom['sunday'] == 'on') {
                    $sundays = ['sundays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSunday();
                    }, $t2)];
                    $duration = $duration + $sundays;
                    $skipped = $skipped + intval($sundays['sundays']);
                    // dd($skipped);
                }
                if ($custom['monday'] == 'on') {
                    $mondays = ['mondays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isMonday();
                    }, $t2)];
                    $duration = $duration + $mondays;
                    $skipped = $skipped + intval($mondays['mondays']);
                }
                if ($custom['tuesday'] == 'on') {
                    $tuesdays = ['tuesdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isTuesday();
                    }, $t2)];
                    $duration = $duration + $tuesdays;
                    $skipped = $skipped + intval($tuesdays['tuesdays']);
                }
                if ($custom['wednesday'] == 'on') {
                    $wednesdays = ['wednesdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isWednesday();
                    }, $t2)];
                    $duration = $duration + $wednesdays;
                    $skipped = $skipped + intval($wednesdays['wednesdays']);
                }
                if ($custom['thursday'] == 'on') {
                    $thursdays = ['thursdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isThursday();
                    }, $t2)];
                    $duration = $duration + $thursdays;
                    $skipped = $skipped + intval($thursdays['thursdays']);
                }
                if ($custom['friday'] == 'on') {
                    $fridays = ['fridays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isFriday();
                    }, $t2)];
                    $duration = $duration + $fridays;
                    $skipped = $skipped + intval($fridays['fridays']);
                }
                if ($custom['saturday'] == 'on') {
                    $saturdays = ['saturdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSaturday();
                    }, $t2)];
                    $duration = $duration + $saturdays;
                    $skipped = $skipped + intval($saturdays['saturdays']);
                }
                $dur = $t1->diffInDays($t2);
                $dur = ['duration' => $dur - $skipped];
                $skipped = ['skipped' => $skipped];
                $duration = $duration + $dur + $skipped;
            }

        }
        else{

            if($request->ach == 1){
                    $skipped = $t1->diffInDaysFiltered(function(Carbon $date) {
                        return !$date->isWeekend();
                    }, $t2);
                    $duration = $t1->diffInDaysFiltered(function(Carbon $date) {
                        return $date->isWeekend();
                    }, $t2);
                    $saturdays = $t1->diffInDaysFiltered(function(Carbon $date) {
                        return $date->isSaturday();
                    }, $t2);
                    $sundays = $t1->diffInDaysFiltered(function(Carbon $date) {
                        return $date->isSundays();
                    }, $t2);
                    $duration = [
                        'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                        'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                        'includeEndDate' => $request->includeEndDate,
                        'calendar_days' => $t1->diffInDays($t2),
                        'duration' => $duration,
                        'skipped' => $skipped,
                        'saturdays' => $saturdays,
                        'sundays' => $sundays,
                        'type' => 'include',
                        'ach' => 'weekends'
                    ];
            }else if ($request->ach == 2) {
                    $duration = $t1->diffInDaysFiltered(function(Carbon $date) {
                        return $date->isSameDay($date);
                    }, $t2);
                   
                    $duration = [
                        'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                        'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                        'includeEndDate' => $request->includeEndDate,
                        'calendar_days' => $t1->diffInDays($t2),
                        'duration' => $duration,
                        // 'skipped' => $t1->diffInDays($t2),
                        
                        'type' => 'include',
                        'ach' => 'alldays'
                    ];
            }else if ($request->ach == 3) {
                 $custom = $request->custom;
                $included = 0;
                $duration = [
                    'startDate' => $t1->isoFormat('dddd, Do MMMM YYYY'),
                    'enddate' => $t2->isoFormat('dddd, Do MMMM YYYY'),
                    'includeEndDate' => $request->includeEndDate,
                    'calendar_days' => $t1->diffInDays($t2),
                    'type' => 'include',
                    'ach' => 'custom'
                ];
                if ($custom['sunday'] == 'on') {
                    $sundays = ['sundays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSunday();
                    }, $t2)];
                    $duration = $duration + $sundays;
                    $included = $included + intval($sundays['sundays']);
                    // dd($included);
                }
                if ($custom['monday'] == 'on') {
                    $mondays = ['mondays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isMonday();
                    }, $t2)];
                    $duration = $duration + $mondays;
                    $included = $included + intval($mondays['mondays']);
                }
                if ($custom['tuesday'] == 'on') {
                    $tuesdays = ['tuesdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isTuesday();
                    }, $t2)];
                    $duration = $duration + $tuesdays;
                    $included = $included + intval($tuesdays['tuesdays']);
                }
                if ($custom['wednesday'] == 'on') {
                    $wednesdays = ['wednesdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isWednesday();
                    }, $t2)];
                    $duration = $duration + $wednesdays;
                    $included = $included + intval($wednesdays['wednesdays']);
                }
                if ($custom['thursday'] == 'on') {
                    $thursdays = ['thursdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isThursday();
                    }, $t2)];
                    $duration = $duration + $thursdays;
                    $included = $included + intval($thursdays['thursdays']);
                }
                if ($custom['friday'] == 'on') {
                    $fridays = ['fridays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isFriday();
                    }, $t2)];
                    $duration = $duration + $fridays;
                    $included = $included + intval($fridays['fridays']);
                }
                if ($custom['saturday'] == 'on') {
                    $saturdays = ['saturdays' => $t1->diffInDaysFiltered(function(Carbon $date) {
                    return $date->isSaturday();
                    }, $t2)];
                    $duration = $duration + $saturdays;
                    $included = $included + intval($saturdays['saturdays']);
                }
                $dur = $t1->diffInDays($t2);
                $dur = ['duration' => $included];
                $included = ['included' => $included];
                $duration = $duration + $dur + $included;
            }
         }


        return response()->json($duration);
    }
  
}