<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\UserCity;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use Illuminate\Support\Facades\Session;


class DashboardController extends Controller
{

    protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }
    public function index()
    {   
     
        return $this->currentTime();
    }

    public function homeLocations()
    {
        $api_city = Http::get('http://ip-api.com/json')->json();
        
        $dt = Carbon::now();
        $dt->timezone($api_city['timezone']);
        $abbreviation = $dt->format('T');
        $timezones = Timezone::where('timezone', $api_city['timezone'])->orWhere('timezone', $abbreviation)->get();
        $cities = array();
        $i = 0;
        foreach ($timezones as $timezone) {
            $city = City::where('timezone_id', $timezones[$i]->id)->first();
            if (!is_null($city)) {
                $cities[$i] = $city;
                $city_timezone = $city->timezone;
                $this->timeService->realTime($city_timezone);         
            }else{}
            $i++;
        }


        return response()->json($cities);
    }
    public function setHomeLocations(Request $request)
    {
        $this->validate($request, [
         'city_id' => 'required|exists:cities,id',
         
        ]);
        //check if a user is logged in 
        if (auth()->user()) {
            $user_city = new UserCity;
            $user_city->city_id = 2;
            $user_city->user_id = auth()->user()->id;
            $user_city->save();

        }
        return redirect()->route('dashboard');
    }
    
    public function currentTime()
    {   
        //check if a user is logged in and that user has a city
        if (auth()->user() && auth()->user()->userCity->last() != null) {
           
            $user = User::find(auth()->user()->id);

            if ($user->userCity->last() != null) {
                $city_id = $user->userCity->last()->city_id;
                $city = City::find($city_id);
                $this->timeService->realTime($city->timezone);
                return response()->json($city);
            }

        }
        else{ 
            $api_city = Http::get('http://ip-api.com/json')->json();

             if ($api_city['status'] == "success") {
                     $country = Country::find(strtolower($api_city['countryCode']));
                     
                     if ($country != null) {
                        $city = City::where('textual_id', $country->textual_id.'/'.strtolower($api_city['city']))->first();
                     }
                     else{
                        $city = City::where('name', $api_city['city'])->first();
                     }

                     if (isset($city)) {
                         $this->timeService->realTime($city->timezone);
                         return response()->json($city);
                     }
                     else{
                        $city = City::where('country_abbreviation', strtolower($api_city['countryCode']))->first();
                        
                        if ($city != null) {
                             $this->timeService->realTime($city->timezone);
                             return response()->json($city);
                        }else{
                         $city = City::all()->random(1);       
                            foreach ($city as $one) {
                            $this->timeService->realTime($one->timezone);
                            }         
                            return response()->json($city);
                        }
                    }
             }else{
                $city = City::all()->random(1);       
                 $this->timeService->realTime($city->timezone);
                 return response()->json($city);
             }
        }

    }
    
}
