<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use Illuminate\Support\Facades\Http;


class CityController extends Controller
{   
	protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }

    public function all()
    {
 	  return $this->locationService->getAllCities();	
    }   

     public function worldclock()
    {
      $cities = City::all();
      foreach ($cities as $city) {
          $timezone = $city->timezone;
          $this->timeService->realTime($timezone);
      }

      return response()->json($cities);    
    }   
    //Return State or City 
  public function stateCity($country, $city)
  {
    $textual_id = $country.'/'.$city;
    $cities = $this->locationService->getCity($textual_id);
    if ($cities->count() < 1) { 
    $states = $this->locationService->getState($textual_id);
    if ($states->count() < 1) {
    return response()->json(['message'=>'Not Found']);
    }else{
      foreach ($states as $state) {
        $cities = $state->city;
        $timezones= $state->timezone;
        $capital= $state->capital->city->first();
        foreach ($timezones as $timezone) {
          $this->timeService->realTime($timezone);
        }
      }
    return response()->json(['state'=>$state]);
    }
    }else{
      foreach ($cities as $city) {
      $this->timeService->realTime($city->timezone);
      }
    return response()->json(['city'=>$city]);
    }
  }

  public function weatherStateCity($country, $city)
  {
     $textual_id = $country.'/'.$city;
    $cities = $this->locationService->getCity($textual_id);

    if ($cities->count() < 1) { 
    $states = $this->locationService->getState($textual_id);
    if ($states->count() < 1) {
    return response()->json(['message'=>'Not Found']);
    }else{
      foreach ($states as $state) {
         $weather = Http::get('http://api.weatherapi.com/v1/current.json?key=802e438aef604943a5a225933202811&q='.$state->name)->json();
        if (isset($weather['error'])) {
          return response()->json($weather);
        }else{
          $info = [];
          $textual_id = ['textual_id' => $state->textual_id];
          $id = ['state_abbreviation' => $state->abbreviation];
          $state = $weather['location'];
          $state = $state + $id;
          $state = $state + $textual_id;
          $current = ['weather' => $weather['current']];
          $state = $state + $current;
        }

      }
    return response()->json(['state'=>$state]);
    }
    }else{
      foreach ($cities as $city) {
      $this->timeService->realTime($city->timezone);
        $weather = Http::get('http://api.weatherapi.com/v1/current.json?key=802e438aef604943a5a225933202811&q='.$city->name)->json();
        if (isset($weather['error'])) {
          return response()->json($weather);
        }else{
          $info = [];
          $textual_id = ['textual_id' => $city->textual_id];
          $id = ['city_id' => $city->id];
          $city = $weather['location'];
          $city = $city + $id;
          $city = $city + $textual_id;
          $current = ['weather' => $weather['current']];
          $city = $city + $current;
        }
      }
    return response()->json(['city'=>$city]);
    }
  }

     public function weather()
    {
      $cities = City::all();
      $weathers = [];
      foreach ($cities as $city) {
          $weather = Http::get('http://api.weatherapi.com/v1/current.json?key=802e438aef604943a5a225933202811&q='.$city->name)->json();
          if (isset($weather['error'])) {
          return response()->json($weather);
          }else{
          $info = [];
          $textual_id = ['textual_id' => $city->textual_id];
          $id = ['city_id' => $city->id];
          $city = $weather['location'];
          $city = $city + $id;
          $city = $city + $textual_id;
          $current = ['weather' => $weather['current']];
          $info = $city + $current;

          array_push($weathers, $info);
      }
      }

      return response()->json($weathers);    

    }  

    


}