<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use App\Models\City;
use App\Models\Event;
use App\Models\User;
use App\Models\Timezone;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;


class EventController extends Controller
{
	protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }

   	public function eventStore(Request $request)
   	{
 /*  		{
    "name": "Event",
    "city_id": "1",
    "date": {
        "day":"2", 
        "month": "02",
        "year": "2019",
        "hour": "8",
        "minute": "59",
        "second": "00"
            },
    "duration": 
            {
        "hours": "12",
        "minutes": "21"
            }
}*/		

   		//user_id
		
		$this->validate($request, [
         'name' => 'required',
         'date.year' => 'required|integer|digits:4',
         'date.month' => 'required|date_format:m',
         'date.day' => 'required|integer|min:1|max:31',
         'date.hour' => 'required|date_format:G',
         'date.minute' => 'required|numeric|min:0|max:59',
         'date.second' => 'required|numeric|min:0|max:59',
         'duration.hours' => 'required|date_format:G',
         'duration.minutes' => 'required|numeric|min:0|max:59',
     	]);

   		$date = $request->date;
   		$duration = $request->duration;

   		$event = new Event;
   		$event->name = $request->name;
   		if (auth()->user() != null) {
   		$event->user_id = auth()->user()->id;
   		}
   		$event->city_id = $request->city_id;
   		$event->day = $date['day'];
   		$event->month = $date['month'];
   		$event->year = $date['year'];
   		$event->hour = $date['hour'];
   		$event->minute = $date['minute'];
   		$event->second = $date['second'];
   		$event->hours = $duration['hours'];
   		$event->minutes = $duration['minutes'];
   		$event->save();

   		if ($event->save()) {
   		$id = $event->id;
   		return redirect()->route('event', compact('id'));
   		}
   	}
   	public function event($id)
   	{	//should find a user timezone to convert this same dates to user timezone
   		// dd($id);

   		$event = Event::find($id);
   		$city = City::find($event->city_id);
   		$timezone = $city->timezone;

   		$now = Carbon::parse($timezone->timezone);
                
   		$start = Carbon::parse(
   			$event['year'].'-'.
            $event['month'].'-'.
            $event['day'].' '.
            $event['hour'].':'.
            $event['minute'].':'.
            $event['second']);
   		$end = Carbon::parse($start)->addHours($event['hours'])->addMinutes($event['minutes']);
   		$result = [
   			'event' => $event,
   			'current_time' => $now->isoFormat('h:mm:ss a'),
   			'current_day' => $now->isoFormat('ddd'),
   			'start_time' => $start->isoFormat('h:mm:ss a'),
   			'start_date' => $start->isoFormat('dddd, MMMM D, YYYY'),
   			'end_time' => $end->isoFormat('h:mm a'),
   		];
      
   		//if user is logged in and has a city
   		if (auth()->user() && auth()->user()->userCity->last() != null) {
           
            $user = User::find(auth()->user()->id);
            if ($user->userCity->last() != null) {
                $user_city_id = $user->userCity->last()->city_id;
                $user_city = City::find($user_city_id);
                // dd($user_city->timezone->abbreviation);
                // $this->timeService->realTime($city->timezone);
                $user_now = $now->timezone($user_city->timezone->abbreviation);
                $start->timezone($user_city->timezone->abbreviation);
                $user_end = Carbon::parse($start)->addHours($event['hours'])->addMinutes($event['minutes']);

                    $user_result = [
                    // 'event' => $event,
                    'current_time' => $user_now->isoFormat('h:mm:ss a'),
                    'current_day' => $user_now->isoFormat('ddd'),
                    'start_time' => $start->isoFormat('h:mm:ss a'),
                    'start_date' => $start->isoFormat('dddd, MMMM D, YYYY'),
                    'end_time' => $user_end->isoFormat('h:mm a'),
                  ];
              
            }
      }


   		return response()->json(['result'=>$result, 'user_result'=>$user_result]);

   	}
   			// abort(401);
}