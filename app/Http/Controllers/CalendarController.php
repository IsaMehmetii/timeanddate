<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Continents;
use App\Models\City;
use App\Models\User;
use App\Models\Country;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;

class CalendarController extends Controller
{   
    protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }
 
    public function yearCalendar(Request $request)
    {
    	/*fields{  "year":"2020",
    			   "country_abbreviation": "ks" 
    			}*/
    	$this->validate($request, [
         'year' => 'required|integer|digits:4',
         'country_abbreviation' => 'required|exists:countries,abbreviation'
        ]);

    	$country = Country::find($request->country_abbreviation);
    	$timezone = $country->timezone[1];
		$date = Carbon::now();
		$date->year($request->year);
		$date->timezone($timezone->abbreviation);
		$yearEnd = Carbon::parse($date)->endOfYear();
		$yearStart = Carbon::parse($date)->startOfYear();
		$yearBefore = Carbon::parse($date)->subYears('1');
		$yearAfter = Carbon::parse($date)->addYears('1');
		
    	$result = [
    		'country_abbreviation' => $request->country_abbreviation,
    		'date' => $date->isoFormat('MMMM Do YYYY'),
    		'day' => $yearEnd->isoFormat('D'),
    		'month' => $date->isoFormat('MM'),
    		'month_text' => $date->isoFormat('MMMM'),
    		'year' => $date->isoFormat('YYYY'),
    		'year_days' => $yearEnd->isoFormat('DDD'),
    		'year_end_date' => $yearEnd->isoFormat('MMMM Do YYYY'),
    		'year_end_day_text' => $yearEnd->isoFormat('dddd'),
    		'year_start_date' => $yearStart->isoFormat('MMMM Do YYYY'),
    		'year_start_day_text' => $yearStart->isoFormat('dddd'),
    		'year_before' => $yearBefore->isoFormat('YYYY'),
    		'year_after' => $yearAfter->isoFormat('YYYY')
    	];
		return response()->json($result);    	
    }

     public function monthCalendar(Request $request)
    {	
    	/*fields{  "year":"2020",
   				   "month":"1",
    				"country_abbreviation": "ks" 
    			}*/
    	$country = Country::find($request->country_abbreviation);
    	$timezone = $country->timezone[1];
		$date = Carbon::now();
		$date->year($request->year);
		$date->month($request->month);
		$date->timezone($timezone->abbreviation);
		$monthEnd = Carbon::parse($date)->endOfMonth();
		$monthStart = Carbon::parse($date)->startOfMonth();
		$monthBefore = Carbon::parse($date)->submonths('1');
		$monthAfter = Carbon::parse($date)->addmonths('1');
		
    	$result = [
    		'date' => $date->isoFormat('MMMM Do YYYY'),
    		'month' => $date->isoFormat('MM'),
    		'month_text' => $date->isoFormat('MMMM'),
    		'year' => $date->isoFormat('YYYY'),
    		'month_days_start' => $monthStart->isoFormat('DDD'),
    		'month_days_start_text' => $monthStart->isoFormat('dddd'),
    		'month_days_end' => $monthEnd->isoFormat('DDD'),
    		'month_days_end_text' => $monthEnd->isoFormat('dddd'),
    		'month_before' => $monthBefore->isoFormat('M'),
    		'month_before_text' => $monthBefore->isoFormat('MMMM'),
    		'month_after' => $monthAfter->isoFormat('M'),
    		'month_after_text' => $monthAfter->isoFormat('MMMM')
    	];
		return response()->json($result);    	
    }
 }
