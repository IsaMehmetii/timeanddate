<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use Illuminate\Support\Facades\Http;

class CountryController extends Controller
{   
	
	protected $timeService;
	protected $locationService;
	function __construct(TimeService $timeService, LocationService $locationService)
	{
		$this->timeService = $timeService;
		$this->locationService = $locationService;
	}

	//Return Country
	public function index($textual_id)
	{
		//get country by textual_id
		$countries = $this->locationService->getCountry($textual_id);
		
		if ($countries->count() < 1) {	
			return response()->json(['message'=>'Not found']);
		}else{
			foreach ($countries as $country) {
				$country->capital->city->first();
				if (isset($country->capital->first()->state_abbreviation)) {
					$country->capital->state()->first();					
					
				}
				$state = $country->state;
				$city = $country->city;
				$timezones = $country->timezone;
				foreach ($timezones as $timezone) {

					$this->timeService->realTime($timezone);		 
				}	
			return response()->json(['country'=>$country]);
			}
		}

	}
	//Return State or City 
	public function stateCity($country, $city)
	{
		$textual_id = $country.'/'.$city;
		$cities = $this->locationService->getCity($textual_id);
		if ($cities->count() < 1) {	
		$states = $this->locationService->getState($textual_id);
		if ($states->count() < 1) {
		return response()->json(['message'=>'Not Found']);
		}else{
			foreach ($states as $state) {
				$cities = $state->city;
				$timezones= $state->timezone;
				$capital= $state->capital->city->first();
				foreach ($timezones as $timezone) {
					$this->timeService->realTime($timezone);
				}
			}
		return response()->json(['state'=>$state]);
		}
		}else{
			foreach ($cities as $city) {
			$this->timeService->realTime($city->timezone);
			}
		return response()->json(['city'=>$city]);
		}
	}

	public function weatherCountry($textual_id)
	{
		//get country by textual_id
		$countries = $this->locationService->getCountry($textual_id);
		
		if ($countries->count() < 1) {	
			return response()->json(['message'=>'Not found']);
		}else{
			foreach ($countries as $country) {
		$weather = Http::get('http://api.weatherapi.com/v1/current.json?key=802e438aef604943a5a225933202811&q='.$country->name)->json();
        if (isset($weather['error'])) {
          return response()->json($weather);
        }else{
          $info = [];
          $textual_id = ['textual_id' => $country->textual_id];
          $id = ['country_abbreviation' => $country->abbreviation];
          $country = $weather['location'];
          $country = $country + $id;
          $country = $country + $textual_id;
          $current = ['weather' => $weather['current']];
          $country = $country + $current;
        }
				
			return response()->json(['country'=>$country]);
			}
		}
		}


	
			
	//all countries
	public function all()
	{
	    return $this->locationService->getAllCountries();
	}
}