<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;

class AuthController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        Config::set('auth.providers.users.model',Admin::class);
        Config::set('auth.providers', ['users' => ['driver' => 'eloquent','model' => Admin::class,]]);    
        // $this->middleware('jwt.auth', ['except' => ['login','register']]);
    }
	public function register(Request $request)
	{
		//validate incoming request 
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:admins',
            'password' => 'required',
        ]);
        
        try {
            // Config::set('jwt.user', "App\Models\Admin");
            // Config::set('auth.providers.users.model', \App\Models\Admin::class);
            $user = new Admin;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->save();
            $token = auth()->tokenById($user->id);
            //return successful response
            return response()->json(['admin' => $user, 'message' => 'CREATED' ,'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 460], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
	}

     // /**
    //  * Get the authenticated User.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    public function me()
    {  
        return response()->json(auth()->user());
    }


	public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);

    }

 
	public function refresh()
    {
         return $this->respondWithToken(auth()->refresh());
    }
	    
      protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 460
        ]);
    }

      public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
	}