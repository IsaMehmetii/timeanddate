<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;

/**
 * 
 */
class CityController extends Controller
{
protected $timeService;
	protected $locationService;
	function __construct(TimeService $timeService, LocationService $locationService)
	{
        Config::set('auth.providers.users.model',Admin::class);
        // $this->middleware('auth');
		$this->timeService = $timeService;
		$this->locationService = $locationService;
	}
	
	public function store(Request $request)
		{
/*			{
			   					| "name": "Albany",
	required   					| "dial_code": "+1",
	required   					| "lat_long": "42°39'N / 73°45'W",
			   					| "elevation": "30m",
validated:exists in states table|  "state_abbreviation": "nys",
validated:exists in countries tb| "country_abbreviation": "usa",
if a city is a capital  		| "capital" : "1",
use 1 for true and 0 for false 
	required   					| "currency": "USD",
not validated    				| "timezone": "EST"
}*/
			//validate request
			$this->validate($request, [
	         'name' => 'required',
   		 	 'dial_code' => 'required|regex:/^\+\d{1,15}$/',
   		 	 'country_abbreviation' => 'sometimes|required|exists:countries,abbreviation',
   		 	 'state_abbreviation' => 'sometimes|required|exists:states,abbreviation',
   		 	 'lat_long' => 'required',
   		 	 'elevation' => 'required',
   		 	 'currency' => 'required',
	        ]);
	        if (Timezone::where('timezone', $request->timezone)->first()) {
	        	$timezone =Timezone::where('timezone', $request->timezone)->first();
	        }else{
				//Store timezone
                $timezone = new Timezone;
               	$this->timeService->storeTime($request->timezone, $timezone);
                $timezone->save();
                }
			$city = new City;
			
			if (isset($request->state_abbreviation)) {
			$city->state_abbreviation = $request->state_abbreviation;
			$state = State::find($request->state_abbreviation);

			//if city is a state-capital
			if ($request->capital == 1) {
			$city->capital_id = $state->capital->id;	
			}

			if (!isset($request->country_abbreviation)) {
			$country = $state->country;
			$city->country_abbreviation = $country->abbreviation;
			$city->textual_id = str_replace(' ', '-', strtolower($country->name.'/'.$request->name));
			$city->continent_id = $country->continent_id;
			}else{
			$country = Country::find($request->country_abbreviation);
			$city->country_abbreviation = $request->country_abbreviation;
			$city->textual_id = str_replace(' ', '-', strtolower($country->name.'/'.$request->name));
			$city->continent_id = $country->continent_id;
			}}else{
			 if (isset($request->country_abbreviation)) {
			$country = Country::find($request->country_abbreviation);

			//if city is a country-capital
			if ($request->capital == 1) {
			$city->capital_id = $country->capital->id;	
			}

			$city->country_abbreviation = $request->country_abbreviation;
			$city->textual_id = str_replace(' ', '-', strtolower($country->name.'/'.$request->name));
			$city->continent_id = $country->continent_id;
			 }else{}
			}
			$city->name = $request->name;
			$city->timezone_id = $timezone->id;
			$city->lat_long = $request->lat_long;
			$city->elevation = $request->elevation;
			$city->dial_code = $request->dial_code;
			$city->currency = $request->currency;
			$city->save();
			$country = $city->country;
			$state = $city->state;
			$timezone = $city->timezone;
			return response()->json(['City'=> $city, 'message'=> 'City Created Successfully']);
		}
		public function update(Request $request, $id)
		{
			$this->validate($request, [
   		 	 'dial_code' => 'required|regex:/^\+\d{1,15}$/',
   		 	 'lat_long' => 'required',
   		 	 'elevation' => 'required',
   		 	 'currency' => 'required',
	        ]);
			$city = $this->locationService->findCity($id);
			if ($city == null) {
				return response()->json(['message' => 'Not Found!']);
			}
            $city->lat_long = $request->lat_long;
			$city->elevation = $request->elevation;
			$city->dial_code = $request->dial_code;
			$city->currency = $request->currency;
            $city->save();

             if($city->save()){
                return response()->json(['message'=> 'City Updated Successfully', 'city'=> $city],500); 
            }else{
                return response()->json('Something went Wrong');
            }
		}
		  public function destroy($id)
        {
            $city = $this->locationService->findCity($id);

			if ($city == null) {

				return response()->json(['message' => 'Not Found!']);

			}

            $city->delete();
            return response()->json(['City deleted Successfully'],203); 

        }
}