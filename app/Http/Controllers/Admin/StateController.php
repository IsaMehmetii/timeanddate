<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Capital;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use Illuminate\Validation\Rule;

/**
 * 
 */
class StateController extends Controller
{
/*    
   /                                    |     {"timezone": 
                                        |            {
    not validated                       |                 "0":"America/New_York", 
                                        |                 "1": "EDT",
                                        |                 "2": "EDT"
   \                                    |            },
    validated:unique in states table    |"abbreviation": "nys",
                                        |"capital_name": "Albany",
if a state is a capital                 | "capital" : "1",
use 1 for true and 0 for false 
                                        |"name": "New York State",
validated:exists in countries table     |"country_abbreviation": "usa",
use state as a default                  |"type": "state"
 }*/



	protected $timeService;
	protected $locationService;
	function __construct(TimeService $timeService, LocationService $locationService)
	{
        Config::set('auth.providers.users.model',Admin::class);
        // $this->middleware('auth');
		$this->timeService = $timeService;
		$this->locationService = $locationService;
	}

	public function store(Request $request)
		{	
        //validate request
        $this->validate($request, [
         'abbreviation' => 'required|unique:states',
         'country_abbreviation' => 'sometimes|required|exists:countries,abbreviation',
         'name' => 'required|unique:states',
         'continent_id' => 'sometimes|required',
         'capital_name' => 'required',
         'type' => 'required',
         // 'timezone.*' => 'required|timezone',
        ]);
            //Store state
            $state = new State;
            
            if (isset($request->country_abbreviation)) {
	            $country = Country::find($request->country_abbreviation);

                //if State is a country-capital
                if ($request->capital == 1) {
                    $capital = $country->capital->first();
                    $capital->state_abbreviation = $request->abbreviation;
                    $capital->save();
                }

                if (isset($country->continent_id)) {
                $continent_id = $country->continent_id;
                $state->continent_id = $continent_id; 
                }
	            $state->country_abbreviation = $request->country_abbreviation;
	            $state->textual_id = str_replace(' ', '-', strtolower($country->name.'/'.$request->name));
            }else{
                    if (isset($request->continent_id)) {
                    $state->continent_id = $request->continent_id;
                    }
                }
	            $state->abbreviation = strtolower($request->abbreviation);
	            $state->name = $request->name;
	            $state->capital_name = $request->capital_name;
	            $state->type = $request->type;
	            $state->save();
            if (isset($request->country_abbreviation)) {
            $country = $state->country;
            }else{} 
            
            //Store timezone
            $i = 0;
            foreach ($request->timezone as $zone) {
                if (Timezone::where('timezone', $request->timezone[$i])->first()) {
                $timezone =Timezone::where('timezone', $request->timezone[$i])->first();
            }else{
                //Store timezone
                $timezone = new Timezone;
                $this->timeService->storeTime($request->timezone[$i], $timezone);
                $timezone->save();
                }
            $i= $i + 1;
            $state->timezone()->attach($timezone->id);
            }
            $timezone = $state->timezone;

            $capital = new Capital;
            $capital->state_abbreviation = $state->abbreviation;
            $capital->save();

            return response()->json(['State'=> $state, 'message'=> 'State Created Successfully']);
		}

        public function update(Request $request, $abbr)
        {
            $state = $this->locationService->findState($abbr);
            
             if ($state == null) {
            return response()->json(['message' => 'Not Found!']);
            }
            //validate request
            $this->validate($request, [
             'name' =>  Rule::unique('states')->ignore($state),
             'type' => 'required',
            ]);

            $state->name = $request->name;
            if (isset($state->country)) {
                $country = $state->country;
             $state->textual_id = str_replace(' ', '-', strtolower($country->name.'/'.$request->name));
                
            }
            $state->type = $request->type;
            $state->save();

            if($state->save()){
                return response()->json([ 'message'=> 'State Updated Successfully', 'state'=> $state],500); 
            }else{
                return response()->json('Something went Wrong');
            }
        }
        public function destroy($abbr)
        {
            $state = $this->locationService->findState($abbr);

             if ($state == null) {
            return response()->json(['message' => 'Not Found!']);
            }
            
            $state->delete();
            return response()->json(['State deleted Successfully'],203); 

        }
}