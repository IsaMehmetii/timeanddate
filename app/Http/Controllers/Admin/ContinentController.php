<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Continents;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use Illuminate\Validation\Rule;

class ContinentController extends Controller
{
    
    protected $timeService;
    protected $locationService;
    function __construct()
    {
        Config::set('auth.providers.users.model',Admin::class);
        // $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
         'name' => 'required|unique:continents',
         'short_name' => 'required|unique:continents',
        ]);
        $continent = Continents::create($request->all()); 
        return response()->json($continent);
    }
    
    public function update(Request $request, $id)
    {   
        $continent = Continents::find($id);
        
        if ($continent == null) {
            return response()->json(['message' => 'Not Found!']);
        }
        
        $this->validate($request, [
         'name' => Rule::unique('continents')->ignore($continent),
         'short_name' => Rule::unique('continents')->ignore($continent)
        ]);

        $continent->name = $request->name;
        $continent->short_name = $request->short_name;
        $continent->save();

        if($continent->save()){
            return response()->json(['continent'=> $continent, 'message'=> 'Continent Updated Successfully'],200); 
        }else{
            return response()->json('Something went Wrong');
        }
    }

    public function destroy($id)
    {
        $continent = Continents::find($id);
        if ($continent == null) {
            return response()->json(['message' => 'Not Found!'],404);
        }
        $continent->delete();
            return response()->json(['message'=>'Continent deleted Successfully'],200); 
    }

   
}
