<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\User;
use App\Models\Capital;
use App\Models\Timezone;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;

/**
 * 
 */
class CountryController extends Controller
{
	protected $timeService;
	protected $locationService;
	function __construct(TimeService $timeService, LocationService $locationService)
	{
        Config::set('auth.providers.users.model',Admin::class);
        // $this->middleware('auth');
		$this->timeService = $timeService;
		$this->locationService = $locationService;
	}
	
	public function store(Request $request)
	{
		//validate request
		$this->validate($request, [
   		 'abbreviation' => 'required|unique:countries',
   		 'long_name' => 'required',
   		 'name' => 'required',
   		 'capital_name' => 'required',
   		 'dial_code' => 'required|regex:/^\+\d{1,15}$/',
   		 'continent_id' => 'sometimes|required|integer',
		]);

		//Store country
		$country = new Country;
		$country->abbreviation = strtolower($request->abbreviation);
		$country->name = $request->name;
		
		//lowecase and Replace whitespaces  
		$country->textual_id = str_replace(' ', '-', strtolower($request->name));
		
		$country->long_name = $request->long_name;

		$country->capital_name = $request->capital_name;

		if (isset($request->continent_id)) {
		$country->continent_id = $request->continent_id;
		}
		if (isset($request->adm_capital)) {
		$country->adm_capital = $request->adm_capital;
		}
		if (isset($request->jud_capital)) {
		$country->jud_capital = $request->jud_capital;
		}
		if (isset($request->leg_capital)) {
		$country->leg_capital = $request->leg_capital;
		}
		$country->dial_code = $request->dial_code;
		$country->save();
		

		if ($country->save()) {
		//Store timezone
		$i = 0;
		foreach ($request->timezone as $zone) {
		    if (Timezone::where('timezone', $request->timezone[$i])->first()) {
                $timezone =Timezone::where('timezone', $request->timezone[$i])->first();
            }else{
                //Store timezone
                $timezone = new Timezone;
                $this->timeService->storeTime($request->timezone[$i], $timezone);
                $timezone->save();
                }
		$i= $i + 1;
		$country->timezone()->attach($timezone->id);
		}

		$capital = new Capital;
		$capital->country_abbreviation = $country->abbreviation;
		$capital->save();

		$continent = $country->continent;
		$timezone = $country->timezone;
		return response()->json(['message'=> 'Country Created Successfully', 'country'=> $country],200); 
		}else{
            return response()->json('Something went Wrong');

		}

	}
	public function update(Request $request, $abbr)
	{		
		//validate request
		$this->validate($request, [
   		 'long_name' => 'required',
   		 'name' => 'required',
   		 'continent_id' => 'sometimes|required|integer',
   		 'dial_code' => 'required|regex:/^\+\d{1,15}$/',
		]);
		$country = $this->locationService->findCountry($abbr);
		
		 if ($country == null) {
            return response()->json(['message' => 'Not Found!']);
        }

		$country->name = $request->name;
		$country->textual_id = str_replace(' ', '-', strtolower($request->name));
		$country->long_name = $request->long_name;
		if (isset($request->continent_id)) {
		$country->continent_id = $request->continent_id;
		}
		if (isset($request->adm_capital)) {
		$country->adm_capital = $request->adm_capital;
		}else{}
		if (isset($request->jud_capital)) {
		$country->jud_capital = $request->jud_capital;
		}else{}
		$country->dial_code = $request->dial_code;
		$country->save();

		if($country->save()){
			return response()->json(['message'=> 'Country Updated Successfully', 'country'=> $country],500); 
		}else{
			return response()->json('Something went Wrong');
		}
	}

	public function destroy($abbr)
	{
		$country = $this->locationService->findCountry($abbr);
		 if ($country == null) {
            return response()->json(['message' => 'Not Found!']);
        }
		$country->delete();
		return response()->json(['Country deleted Successfully'],500); 


	}



}