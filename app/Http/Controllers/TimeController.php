<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use App\Models\City;
use App\Models\Timezone;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;


class TimeController extends Controller
{
    protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }
    public function timeZones(){
        //get timezones with: city,state,country,continent
            $timezones = Timezone::all();
            foreach ($timezones as $timezone) {
            $this->timeService->realTime($timezone);
            $city = $timezone->city;
            $state = $timezone->state;
            $country = $timezone->country;
            }
        return response()->json($timezones);

    }
    public function timeZonesMap(){
        //get timezones with city and city:state,country,continent
            $timezones = Timezone::all();
            foreach ($timezones as $timezone) {
            $this->timeService->realTime($timezone);
            $cities = $timezone->city;
            foreach ($cities as $city) {
                $continent = $city->continent;
                $country = $city->country;
                $state = $city->state;
            }
            }
        return response()->json($timezones);

    }


    public function dateAdd(Request $request)
    {
           //validate request
        $this->validate($request, [
         'date.year' => 'sometimes|integer|digits:4',
         'date.month' => 'sometimes|date_format:m',
         'date.day' => 'sometimes|integer|min:1|max:31',
         'date.hour' => 'sometimes|date_format:G',
         'date.minute' => 'sometimes|numeric|min:0|max:59',
         'date.second' => 'sometimes|numeric|min:0|max:59',
         'endDate' => 'required|array|min:0',
         'endDate.year' => 'sometimes|integer',
         'endDate.month' => 'sometimes|integer',
         'endDate.day' => 'sometimes|integer',
         'type' => ['required', Rule::in(['sub', 'add'])],
         'repeat' => 'required|integer'
         
        ]);
        if ($this->timeService->emptyElementExists($request->date)) {
            $dt = Carbon::now();
            $date = $request->date;
            if ($date['year'] == '') {
                $date['year'] = $dt->isoFormat('YYYY');
            }
            if ($date['month'] == '') {
                $date['month'] = $dt->isoFormat('MM');
            }
            if ($date['day'] == '') {
                $date['day'] = $dt->isoFormat('DD');
            }
              if ($date['hour'] == '') {
                $date['hour'] = $dt->isoFormat('H');
            }
              if ($date['minute'] == '') {
                $date['minute'] = $dt->isoFormat('m');
            }  
              if ($date['minute'] == '') {
                $date['minute'] = $dt->isoFormat('s');
            }
            $startDate = $date;

        }else{
            $startDate = $request->date;
        }

       $endDate = $request->endDate;
       $t1 =Carbon::parse($this->timeService->parseStart($startDate));
       $t2  = [];
       $value = $t1; 
       $min = Carbon::minValue();
       $max = Carbon::maxValue();    
       for ($i=1; $i <= $request->repeat ; $i++) { 
                                                                   
           if ($value->gt($max) || $value->lt($min)) {
              if ($request->type == 'add') {
                return response()->json('The resulting date is after year 3999. This calculator only supports dates between year 1 and 9999.');
              }else{
                return response()->json('The resulting date is before year 1. This calculator only supports dates between year 1 and 9999.');
              }
            }
            else{

                if ($request->type == 'add') {
                    $value = Carbon::parse($value)
                        ->addDays($request->endDate['day'])
                        ->addMonths($request->endDate['month'])
                        ->addYears($request->endDate['year'])
                        ->addHours($request->endDate['hour']) 
                        ->addMinutes($request->endDate['minute']) 
                        ->addSeconds($request->endDate['second']); 
                }else{
                    $value = Carbon::parse($value)
                        ->subDays($request->endDate['day'])
                        ->subMonths($request->endDate['month'])
                        ->subYears($request->endDate['year'])
                        ->subHours($request->endDate['hour']) 
                        ->subMinutes($request->endDate['minute']) 
                        ->subSeconds($request->endDate['second']); 

                }
                array_push($t2, $value->isoFormat('dddd, Do MMMM YYYY, H:m:s'));
            } 
        }
        if ($request->type == 'add') {
        $info = [
            'startTime'  => $t1->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
            'yearsAdded' => $request->endDate['year'], 
            'monthsAdded' => $request->endDate['month'], 
            'daysAdded' => $request->endDate['day'], 
            'hoursAdded' => $request->endDate['hour'], 
            'minutesAdded' => $request->endDate['minute'], 
            'secondsAdded' => $request->endDate['second'],
        ];
        $added = [ 'text'=>'Added '.$request->endDate['year'].' years, '.$request->endDate['month'].' months, '.$request->endDate['day'].' days (repeated '.$request->repeat.' times)'];
        $info = $info + $added;       
          
        }else{
             $info = [
            'startTime'  => $t1->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
            'yearsSubtracted' => $request->endDate['year'], 
            'monthsSubtracted' => $request->endDate['month'], 
            'daysSubtracted' => $request->endDate['day'], 
            'hoursSubtracted' => $request->endDate['hour'], 
            'minutesSubtracted' => $request->endDate['minute'], 
            'secondsSubtracted' => $request->endDate['second'],
        ];
        $subtracted = [ 'text'=>'subtracted '.$request->endDate['year'].' years, '.$request->endDate['month'].' months, '.$request->endDate['day'].' days (repeated '.$request->repeat.' times)'];
        $info = $info + $subtracted; 
        }

        $i= 1;
       foreach ($t2 as $result) {
        $info = $info + ['result '.$i => $result];
        $i++;
       }
         return response()->json(['info' => $info]);
    }

    public function weekDay(Request $request)
    {
            //validate request
        $this->validate($request, [
         'date.year' => 'required|integer|digits:4',
         'date.month' => 'required|date_format:m',
         'date.day' => 'required|integer|min:1|max:31',
        ]);

        $date = $request->date;
        $dt = Carbon::parse(  
            $date['year'].'-'.
            $date['month'].'-'.
            $date['day']);
        
        $dt2 =  Carbon::parse(  
            $date['year'].'-'.
            $date['month'].'-'.
            $date['day']);
        $lastDayOfMonth = $dt2->endOfMonth();

        // dd($endOfYear);
        // echo 'Days in the year: ', ($dt->isLeapYear() ? 366 : 365);
        $daysleft = $dt2->endOfYear()->diffInDays($dt);

        $last = $dt2->isoFormat('DD MMMM YYYY');
        $result = [
            'text' => $dt->isoFormat('DD MMMM YYYY [is a] dddd'),
            'day' => $dt->isoFormat('DD'),          
            'month' => $dt->isoFormat('MMMM'),
            'year' => $dt->isoFormat('YYYY'),
            'year_days' => $dt2->endOfYear()->isoFormat('DDD'),
            'year_weeks' => $dt2->isoWeeksInYear(),
            'addition' => [
                'day_year' => 'It is the day number '.$dt->isoFormat('DDD').' of the year, '.$daysleft.' days left.',
                'week_of_year' => 'It is '.$dt->isoFormat('dddd').' number '.$dt->weekOfYear.' out of '.$dt2->isoWeeksInYear().' in '.$dt->isoFormat('YYYY') ,
                'week_of_month' => 'It is '.$dt->isoFormat('dddd').' number '.$dt->weekOfMonth.' out of '.$dt2->weekOfMonth.' in '.$dt->isoFormat('MMMM'),
                'year_day' => $dt->isoFormat('[Year] YYYY [has ]').$dt2->endOfYear()->isoFormat('DDDD').' days.',
                'month_day' => $dt->isoFormat('MMMM YYYY [has ]').$dt2->endOfMonth()->isoFormat('D').' days.',
                
            ],

        ];

        return response()->json($result);
    }
    public function weekNumber(Request $request)
    {
        if (isset($request->date)) {
        $date = $request->date;     
        $dt = Carbon::parse(  
            $date['year'].'-'.
            $date['month'].'-'.
            $date['day']);
            
        }else if(isset($request->week_date)){
        $week_date = $request->week_date;     
        $dt = Carbon::now();
        $dt->setISODate($week_date['year'],$week_date['week']);
        }else{
            $dt = Carbon::now();
        }

        
        $info = [
            'week_of_year'=> $dt->weekOfYear.'/'.$dt->isoFormat('YYYY'),
            'start_of_weeek'=> $dt->startOfWeek()->isoFormat('MMMM DD, YYYY'),
            'end_of_weeek'=> $dt->endOfWeek()->isoFormat('MMMM DD, YYYY'),
        ];
        return response()->json($info);

    }

    // public function workDaysAdd(Request $request)
    // {
    //    //daysType - 0-include 1-exclude
    //     //arch - 1-weekends 2-allDays 3-custon

    //          //validate request
    //     $this->validate($request, [
    //      'date.*' => 'required',
    //      'date.year' => 'sometimes|integer|digits:4',
    //      'date.month' => 'sometimes|date_format:m',
    //      'date.day' => 'sometimes|integer|min:1|max:31',
    //      'date.hour' => 'sometimes|date_format:G',
    //      'date.minute' => 'sometimes|numeric|min:0|max:59',
    //      'date.second' => 'sometimes|numeric|min:0|max:59',
    //      'type' => ['required', Rule::in(['sub', 'add'])],
    //      'repeat' => 'required|integer'
    //     ]);
       
    //    $date = $request->date;

    //    $t1 =Carbon::parse(
    //         $date['year'].'-'.
    //         $date['month'].'-'.
    //         $date['day']);

    //    $t2  = [];
    //    $diffs = [];
    //    $value = $t1; 
    //    $min = Carbon::minValue();
    //    $max = Carbon::maxValue();    
    //    for ($i=1; $i <= $request->repeat ; $i++) { 
                                                                   
    //        if ($value->gt($max) || $value->lt($min)) {
    //           if ($request->type == 'add') {
    //             return response()->json('The resulting date is after year 3999. This calculator only supports dates between year 1 and 9999.');
    //           }else{
    //             return response()->json('The resulting date is before year 1. This calculator only supports dates between year 1 and 9999.');
    //           }
    //         }else{
    //             if ($request->type == 'add') {
    //                 $before = $value;
    //                 $value = Carbon::parse($value)
    //                     ->addDays($request->days);

    //               // include
    //               if ($request->daysType == 0) {
    //                   // include-weekends
    //                   if ($request->ach == 1) {
    //                 $diff = $value->diffInDaysFiltered(function(Carbon $date) {
    //                 return !$date->isWeekend();
    //                 }, $before);
    //                  $saturdays = $value->diffInDaysFiltered(function(Carbon $date) {
    //                 return $date->isSaturday();
    //                 }, $before);
    //                 $sundays = $value->diffInDaysFiltered(function(Carbon $date) {
    //                     return $date->isSunday();
    //                 }, $before);
    //                   }

    //                 //exclude days
    //               }else{
    //                  if ($request->ach == 1) {

    //                    $saturdays = $value->diffInDaysFiltered(function(Carbon $date) {
    //                 return $date->isSaturday();
    //                 }, $before);
    //                 $sundays = $value->diffInDaysFiltered(function(Carbon $date) {
    //                     return $date->isSunday();
    //                 }, $before);
    //                  }
    //               }

    //             }else{
    //                 $value = Carbon::parse($value)
    //                     ->subDays($request->days);
    //             }
    //             $difference = [$i-1 => ['diff' => $diff,
    //                             'saturdays' => $saturdays,
    //                             'sundays' => $sundays]];
    //             $diffs = $diffs + $difference;
    //             array_push($t2, $value->isoFormat('dddd, Do MMMM YYYY, H:m:s'));
    //         } 
    //     }
    //     // dd($diffs);
    //     if ($request->type == 'add') {
    //     $info = [
    //         'startTime'  => $t1->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
    //         'daysAdded' => $request->days, 
    //     ];
          
    //     }else{
    //          $info = [
    //         'startTime'  => $t1->isoFormat('dddd, Do MMMM YYYY, H:m:s'),
    //         'daysSubtracted' => $request->days, 
    //     ];
    //     }

    //     $i= 1;
    //    foreach ($t2 as $result) {
    //     $info = $info + ['result '.$i => $result];
    //     // $diff = $diff + ['result '.$i => $result];
    //     $i++;
    //    }
    //      return response()->json(['info' => $info]);
    // }
  
}


