<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Carbon\CarbonImmutable;
use App\Services\TimeService;
use App\Services\LocationService;
use App\Models\City;
use App\Models\User;
use App\Models\CountDown;
use App\Models\Timezone;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;
use Validator;


class CountDownController extends Controller
{
    protected $timeService;
    protected $locationService;
    function __construct(TimeService $timeService, LocationService $locationService)
    {
        $this->timeService = $timeService;
        $this->locationService = $locationService;
    }

      public function countNewYear()
    {   

        $api_city = Http::get('http://ip-api.com/json')->json();
        // dd($api_city);
        if (isset($api_city)) {
        if ($api_city['status'] == 'success') {
        $today = Carbon::now();
        $today->timezone($api_city['timezone']); 
        }else{
        $today = Carbon::now();
        }
        }else{
        $today = Carbon::now();
        }
        $new_year = Carbon::parse($today);
        

        $init =$today->diffInSeconds($new_year->endOfYear());
        $day = floor($init / 86400);
        $hours = floor(($init -($day*86400)) / 3600);
        $minutes = floor(($init / 60) % 60);
        $seconds = $init % 60;

        $duration = [
            'years' => $today->diffInYears($new_year->endOfYear()), 
            'months' => $today->diffInMonths($new_year->endOfYear()),
            'inweeks' => $today->diffInWeeks($new_year->endOfYear()),
            'indays' => $today->diffInDays($new_year->endOfYear()),
            'inhours' => $today->diffInHours($new_year->endOfYear()),
            'inminutes' => $today->diffInMinutes($new_year->endOfYear()), 
            'inseconds' => $today->diffInSeconds($new_year->endOfYear()),
            'days' => $day,
            'hours' => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds,
            'text' => $today->diffForHumans($new_year->endOfYear()),
            'date' => $day.' days '.$hours.' hours '.$minutes.' minutes '.$seconds.' seconds to New Year',
        ];
        return response()->json($duration);
    }

      public function countCreate(Request $request)
      {
        
      	$this->validate($request, [
         'date.year' => 'required|integer|digits:4',
         'date.month' => 'required|date_format:m',
         'date.day' => 'required|integer|min:1|max:31',
         'date.hour' => 'required|date_format:G',
         'date.minute' => 'required|numeric|min:0|max:59',
         'date.second' => 'required|numeric|min:0|max:59',
     	]);	
        	$countdown = new CountDown;

      	if (auth()->user()) {
      		$user_id =  auth()->user()->id;
      		$countdown->user_id = $user_id;
      	}

      	if (isset($request->city_id)) {
      		$city = City::find($request->city_id);
      		$countdown->city_id = $city->id;
      		$timezone = $city->timezone;
      		$this->timeService->realTime($timezone);
      		$today = Carbon::parse($timezone->abbreviation);
      	}else{
      		$api_city = Http::get('http://ip-api.com/json')->json();
	        if (isset($api_city)) {
	        if ($api_city['status'] == 'success') {
	        $today = Carbon::now();
	        $today->timezone($api_city['timezone']); 
	        }else{
	        $today = Carbon::now();
	        }
	        }else{
	        $today = Carbon::now();
        		}
      	}
      		$date = $request->date;
        	$countdt = Carbon::parse(
            $date['year'].'-'.
            $date['month'].'-'.
            $date['day'].' '.
            $date['hour'].':'.
            $date['minute'].':'.
            $date['second']);
        	
	       	$init =$today->diffInSeconds($countdt);
	        $day = floor($init / 86400);
	        $hours = floor(($init -($day*86400)) / 3600);
	        $minutes = floor(($init / 60) % 60);
	        $seconds = $init % 60;



        	$countdown->design = $request->design;
        	$countdown->title = $request->title;
        	$countdown->font_style = $request->font_syle;
        	$countdown->day = $date['day'];
        	// dd($countdown->day);
        	$countdown->month = $date['month'];
        	$countdown->year= $date['year'];
        	$countdown->hour = $date['hour'];
        	$countdown->minute = $date['minute'];
        	$countdown->second = $date['second'];
        	$countdown->save();
        	$id = $countdown->id;

	        return redirect()->route('countdown', compact('id'));

      }
      public function countdown($id)
      {	  
      	 $validator = Validator::make(['id' => $id], [
      		'id' => 'required|exists:count_downs'
   		 ]);
      	 // dd($validator->fails());
      	 if ($validator->fails()) {
      		return response()->json(['success' => false, 'errors' => $validator->messages()], 422);

    	}else{
      	$countdown = CountDown::find($id);

      	// return response()->json($countdown);
      	if (isset($countdown['city_id'])) {
      		$city = City::find($countdown['city_id']);
      		$timezone = $city->timezone;
      		$this->timeService->realTime($timezone);
      		$today = Carbon::parse($timezone->abbreviation);
      	}else{
      		$api_city = Http::get('http://ip-api.com/json')->json();
	        if (isset($api_city)) {
	        if ($api_city['status'] == 'success') {
	        $today = Carbon::now();
	        $today->timezone($api_city['timezone']); 
	        }else{
	        $today = Carbon::now();
	        }
	        }else{
	        $today = Carbon::now();
        		}
        	}

        	$countdt = Carbon::parse(
            $countdown['year'].'-'.
            $countdown['month'].'-'.
            $countdown['day'].' '.
            $countdown['hour'].':'.
            $countdown['minute'].':'.
            $countdown['second']);
       
        	$init =$today->diffInSeconds($countdt);
	        $day = floor($init / 86400);
	        $hours = floor(($init -($day*86400)) / 3600);
	        $minutes = floor(($init / 60) % 60);
	        $seconds = $init % 60;
       		
        	$duration = [
            'years' => $today->diffInYears($countdt), 
            'months' => $today->diffInMonths($countdt),
            'inweeks' => $today->diffInWeeks($countdt),
            'indays' => $today->diffInDays($countdt),
            'inhours' => $today->diffInHours($countdt),
            'inminutes' => $today->diffInMinutes($countdt), 
            'inseconds' => $today->diffInSeconds($countdt),
            'days' => $day,
            'hours' => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds,
            'text' => $today->diffForHumans($countdt),
            'date' => $day.' days '.$hours.' hours '.$minutes.' minutes '.$seconds.' seconds',
        ];

      	return response()->json(['duration'=> $duration, 'countdown'=>$countdown]);
      }
  }

}