<?php
use Illuminate\Validation\Rules\Exists;
use Illuminate\Http\Request;
use App\Models\Timezone;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
| 
*/
   $router->get('/',['as' => 'dashboard', 'uses'=> 'DashboardController@index']);

   //get locations nearby -finished
   $router->get('/getHomeLocations', 'DashboardController@HomeLocations');
   //set user-location -finished
   $router->post('/setHomeLocation', 'DashboardController@SetHomeLocations'); 
$router->group(['prefix' => 'user'], function () use ($router) {
   $router->post('register', 'AuthController@register');
   $router->post('login', 'AuthController@login');
   $router->post('logout', 'AuthController@logout');
   $router->post('refresh', 'AuthController@refresh');
   $router->get('me', 'AuthController@me');
});

    $router->get('test/{url}', 'ContinentController@test');
$router->group(['prefix' => 'worldclock'], function () use ($router) {
   $router->get('/', 'CityController@worldclock');
   $router->get('/continent/{id}', 'ContinentController@cities');
   
   //converter user_id - haven't done it
   // $router->get('/converter', 'WorldClockController@convert');
 	
    $router->get('countries/textual_id', 'CountryController@all');
 	 $router->get('states/textual_id', 'StateController@all');
 	 $router->get('cities/textual_id', 'CityController@all');
    //user_id
   $router->post('/fixedform', 'EventController@eventStore');
   $router->get('/fixedtime/{id}',['as' => 'event', 'uses'=> 'EventController@event']);
   
   $router->get('/{textual_id}', 'CountryController@index');
   $router->get('/{country}/{city}', 'CountryController@stateCity');


  });

$router->group(['prefix' => 'weather'], function () use ($router) {
   $router->get('/', 'CityController@weather');
   $router->get('/{textual_id}', 'CountryController@weatherCountry');
   $router->get('/{country}/{city}', 'CityController@weatherStateCity');

});
$router->group(['prefix' => 'time'], function () use ($router) {
   $router->get('/zones', 'TimeController@timeZones');
   $router->get('/map', 'TimeController@timeZonesMap');
});

//working at documentation - finished
 $router->group(['namespace' => 'Admin','prefix' => 'admin/'], function () use ($router) {
   $router->post('register', 'AuthController@register');
   $router->post('login', 'AuthController@login');
   $router->get('me', 'AuthController@me');
   $router->post('refresh', 'AuthController@refresh');
   $router->post('logout', 'AuthController@logout');
   //Continent routes
   $router->post('continent/store', 'ContinentController@store');
   $router->put('continent/{id}', 'ContinentController@update');
   $router->delete('continent/{id}', 'ContinentController@destroy');
   // //Country routes
   $router->post('country/store', 'CountryController@store');
   $router->put('country/{abbr}', 'CountryController@update');
   $router->delete('country/{abbr}', 'CountryController@destroy');
   // //State routes
   $router->post('state/store', 'StateController@store');
   $router->put('state/{abbr}', 'StateController@update');
   $router->delete('state/{abbr}', 'StateController@destroy');
   // //City routes
   $router->post('city/store', 'CityController@store');
   $router->put('city/{id}', 'CityController@update');
   $router->delete('city/{id}', 'CityController@destroy');
 });
//working at documentation - finished

 $router->group(['prefix' => 'date'], function () use ($router) {
   $router->get('duration', 'DurationController@duration');
   $router->get('timeduration', 'DurationController@timeDuration');
   $router->get('timezoneduration', 'DurationController@timezoneDuration');
   $router->get('workdays', 'DurationController@workDaysDuration');
   $router->get('dateadd', 'TimeController@dateAdd');
   $router->get('weekdayadd', 'TimeController@workDaysAdd');
   $router->get('weekday', 'TimeController@weekDay');
   $router->get('weeknumber', 'TimeController@weekNumber');
});
 $router->group(['prefix' => 'countdown'], function () use ($router) {
   $router->get('/newyear', 'CountDownController@countNewYear');
   //user_id
   $router->post('/create', 'CountDownController@countCreate');
   $router->get('/{id}',['as' => 'countdown', 'uses'=> 'CountDownController@countDown']);

});
 $router->group(['prefix' => 'calendar'], function () use ($router) {
   $router->get('/', 'CalendarController@yearCalendar');
   $router->get('/monthly', 'CalendarController@monthCalendar');

});

